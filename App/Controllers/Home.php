<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;
use App\Models\Review;

/**
 * Home controller
 *
 */
class Home extends Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $reviews = Review::getAll();
//        echo '<pre>',print_r($reviews),'</pre>';exit;
        View::renderTemplate('Home/index.html', $reviews);
    }
}
