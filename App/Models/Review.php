<?php

namespace App\Models;

use Core\Model;
use PDO;

/**
 * Reviews
 *
 */
class Review extends Model
{

    /**
     * Get all the reviews as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db   = static::getDB();
        $stmt = $db->query('SELECT * FROM reviews');

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
